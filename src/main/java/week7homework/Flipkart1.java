package week7homework;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class Flipkart1 {

	        @Test
			public void flipkart() throws InterruptedException {
				
				String url = "https://www.flipkart.com/";
				String menu = "Electronics";
				String mobileModel = "Mi"; //Realme
				
				// Launch browser, set implicit wait, maximize and load URL 
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				ChromeDriver driver = new ChromeDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				driver.get(url);
				
				// Close the login pop
				driver.findElementByXPath("//button[text()='✕']").click();
				
				// Mouse over Menu and click the product
				WebElement electronics = driver.findElementByXPath("//span[text()='"+menu+"']");
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.elementToBeClickable(electronics));
				Actions builder = new Actions(driver);
				builder.moveToElement(electronics).build().perform();
				driver.findElementByLinkText(mobileModel).click();
				
				// Click Newest First
				driver.findElementByXPath("//div[text()='Newest First']").click();
				Thread.sleep(2000); // wait for sometimes
				
				// Get all Mobile and Prices
				List<WebElement> allMobiles = driver.findElementsByClassName("_3wU53n");
				List<WebElement> allPrices = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");
				for (int i = 0; i < allMobiles.size(); i++) {
					System.out.println("The mobile model "+allMobiles.get(i).getText()+" has a price of "+allPrices.get(i).getText());
				}
				
				// Get the first mobile model and click on it
				String mobileModelName = driver.findElementByXPath("//div[text()='Newest First']/following::div[@class='_3wU53n']").getText();
				driver.findElementByXPath("//div[text()='Newest First']/following::div[@class='_3wU53n']").click();
				
				// Switch to the new window
				Set<String> windowHandles = driver.getWindowHandles();
				List<String> allWindows = new ArrayList<>(windowHandles);
				driver.switchTo().window(allWindows.get(1));
				
				// Verify the title of the new window
				if(driver.getTitle().contains(mobileModelName)) {
					System.out.println("Title Matches");
				}else {
					System.out.println(driver.getTitle());
					System.out.println("Title does not match");
				}
				
				// Get the ratings and reviews
				String ratings = driver.findElementByXPath("//span[contains(text(),'Ratings')]").getText();
				System.out.println(ratings);
				String reviews = driver.findElementByXPath("//span[contains(text(),'Reviews')]").getText();
				System.out.println(reviews);
				
				// Close the browser
				driver.quit();
				
			}

		}
